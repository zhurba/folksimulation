#pragma once

#include <memory>
#include <set>
#include <map>
#include <mutex>
#include <deque>

#include "Person.h"
#include "Marriage.h"
#include "Profession.h"

class EmploymentCard
{
public:
    std::shared_ptr<Person> m_person;
    size_t m_queueOrder;
};

class Simulation
{
public:
    Simulation();
    ~Simulation();

    void GenerateStartPopulation(const int countOfPeople);
    void AddPerson(std::shared_ptr<Person> person, const int year);
    void GenerateBirths(const int year);
    void GenerateMarriages(const int year);
    void CalcVacancies();
    Profession GiveProfession();
    bool GainProfession(std::shared_ptr<Person> person, const int year);
    void GainProfessions(const int year);
    void ClearLaborRemains(std::deque<std::shared_ptr<EmploymentCard>>& container);
    void RemovePerson(std::shared_ptr<Person> person);
    void Retire(const int year);
    void GeneratePopulation(const int countOfPeople, const int countOfYears);

    void PrintCommand(const std::string& commandLine) const;
    void PrintPopulation(const int year, const bool alive, const bool professionals) const;
    void PrintPairs(const int year, const bool alive) const;
    void PrintPerson(const std::string& name) const;
    void PrintPerson(const std::shared_ptr<Person>& person, const bool details) const;

private:
    std::set<std::shared_ptr<Person>> m_people;
    std::set<std::shared_ptr<Person>> m_alive; // for alive

    std::set<std::shared_ptr<Person>, PersonCompareByAge> m_singleMen;
    std::set<std::shared_ptr<Person>, PersonCompareByAge> m_singleWomen;
    std::set<std::shared_ptr<Marriage>> m_marriages;

    std::deque<std::shared_ptr<EmploymentCard>> m_unemployed; // for alive
    std::deque<std::shared_ptr<EmploymentCard>> m_employed; // for alive
    std::map<EProfession, int> m_numberOfProfessionals; // for alive

    mutable std::mutex m_mutexGeneration;
    mutable std::mutex m_mutexRemove;
    mutable std::condition_variable m_cvPrint;
    bool m_ready = false;
};

