#include "Marriage.h"
#include "Social.h"
#include "Simulation.h"

Marriage::Marriage(std::shared_ptr<Person> husband, std::shared_ptr<Person> wife, const int year)
 : m_husband(husband)
 , m_wife(wife)
{
    husband->m_spouse = wife;
    wife->m_spouse = husband;
    m_husband->m_yearOfMarriage = year;
    m_wife->m_yearOfMarriage = year;
    m_maxChildren = rand() % g_maxChildrenTotal + 1;
}

Marriage::~Marriage()
{
}

std::shared_ptr<Person> Marriage::ProduceChild(const int year)
{
    std::shared_ptr<Person> child = nullptr;
    if (m_children.size() >= m_maxChildren)
        return child;

    if (rand() % 2 == 0)
        return child;

    if (m_husband->IsAlive(year) && m_wife->IsAlive(year) && m_husband->Age(year) <= g_maxBabyBirthAge && m_wife->Age(year) <= g_maxBabyBirthAge)
    {
        child = Social::GeneratePerson(year, false);
        child->m_father = m_husband;
        child->m_mother = m_wife;
        m_children.insert(child);
    }
    return child;
}