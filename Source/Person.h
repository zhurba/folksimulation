#pragma once

#include <memory>
#include <vector>
#include <string>
#include <mutex>
#include "Data.h"

enum class Gender
{
    Female = 0,
    Male
};

enum class Status
{
    Free = 0,
    Noble,
    Slave
};

class Marriage;

class Person
{
public:
    Person(std::string name, Gender gender, int yearOfBirth, int yearOfDeath, Status status);
    virtual ~Person();

    bool IsAlive(const int year) const;
    bool IsAdult(const int year) const;
    bool IsReadyForMarriage(const int year) const;
    bool IsMarried(const int year) const;
    int Age(const int year) const;

    bool IsEmployed() const;
    bool IsProfessional() const;

    // features:
    std::string m_name;
    Gender m_gender;
    int m_yearOfBirth;
    int m_yearOfMarriage;
    int m_yearOfDeath;

    Status m_status;
    Profession m_profession;

    // relations:
    std::shared_ptr<Person> m_father;
    std::shared_ptr<Person> m_mother;
    std::shared_ptr<Person> m_spouse;
    std::shared_ptr<Marriage> m_marriage;

private:
    mutable std::recursive_mutex m_mutexSelf;
};

struct PersonCompareByAge
{
    bool operator()(const std::shared_ptr<Person>& p1, const std::shared_ptr<Person>& p2) const
    {
        if (p1->m_yearOfBirth == p2->m_yearOfBirth)
            return p1.get() < p2.get();
        else
            return p1->m_yearOfBirth < p2->m_yearOfBirth;
    }
};