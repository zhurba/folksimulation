#include <algorithm>

#include "Social.h"



Social::Social()
{
}


Social::~Social()
{
}

std::string Social::GetRandomName(Gender gender)
{
    if (gender == Gender::Female)
    {
        return femaleNames[rand() % femaleNames.size()];
    }
    else
    {
        return maleNames[rand() % maleNames.size()];
    }
}

Gender Social::GetRandomGender()
{
    int gender = rand() % 2;
    return (gender == 0) ? Gender::Female : Gender::Male;
}

int Social::GetRandomAge(int maxAge)
{
    return rand() % maxAge;
}

int Social::GetRandomMaxAge()
{
    return (rand() % 5 + rand() % 5) * 10 + rand() % 10 + 1;
}

Status Social::GetRandomStatus()
{
    int status = rand() % 3;
    if (status == 0)
    {
        return Status::Free;
    }
    else if (status == 1)
    {
        return Status::Noble;
    }
    else
    {
        return Status::Slave;
    }
}

std::string Social::GenderToString(Gender gender)
{
    return (gender == Gender::Female) ? "female" : "male";
}

std::string Social::StatusToString(Status status)
{
    if (status == Status::Free)
    {
        return "free";
    }
    else if (status == Status::Noble)
    {
        return "noble";
    }
    else
    {
        return "slave";
    }
}

std::shared_ptr<Person> Social::GeneratePerson(int yearOfBirth, const bool isStartPopulation)
{
    Gender gender = Social::GetRandomGender();
    int maxAge = Social::GetRandomMaxAge();
    if (isStartPopulation)
    {
        yearOfBirth = -Social::GetRandomAge(maxAge);
    }
    return std::move(std::make_shared<Person>(Social::GetRandomName(gender), gender, yearOfBirth, yearOfBirth + maxAge, Social::GetRandomStatus()));
}

std::shared_ptr<Marriage> Social::Marry(std::shared_ptr<Person> husband, std::set<std::shared_ptr<Person>, PersonCompareByAge> wifes, const int year)
{
    std::shared_ptr<Marriage> marriage = nullptr;
    if (husband->IsReadyForMarriage(year))
    {
        auto findByAge = [husband, year](const std::shared_ptr<Person>& wife)
        {
            return (abs(husband->m_yearOfBirth - wife->m_yearOfBirth) < g_maxMarriageDiff && wife->IsReadyForMarriage(year));
        };
        auto iter = std::find_if(wifes.begin(), wifes.end(), findByAge);
        if (iter != wifes.end())
        {
            std::shared_ptr<Person> wife = *iter;
            marriage = std::make_shared<Marriage>(husband, wife, year);
            husband->m_marriage = marriage;
            wife->m_marriage = marriage;
        }
    }
    return marriage;
}
