#include "Person.h"
#include "Simulation.h"

Person::Person(std::string name, Gender gender, int yearOfBirth, int yearOfDeath, Status status)
: m_name(name)
, m_gender(gender)
, m_yearOfBirth(yearOfBirth)
, m_yearOfMarriage(-1)
, m_yearOfDeath(yearOfDeath)
, m_status(status)
, m_profession("", EProfession::None, 0)
, m_father(nullptr)
, m_mother(nullptr)
, m_spouse(nullptr)
{
}

Person::~Person()
{
}

bool Person::IsAlive(const int year) const
{
    std::lock_guard<std::recursive_mutex> lock(m_mutexSelf);
    return m_yearOfBirth <= year && year <= m_yearOfDeath;
}

bool Person::IsAdult(const int year) const
{
    std::lock_guard<std::recursive_mutex> lock(m_mutexSelf);
    return Age(year) >= g_maturityAge;
}

bool Person::IsReadyForMarriage(const int year) const
{
    std::lock_guard<std::recursive_mutex> lock(m_mutexSelf);
    return IsAlive(year) && IsAdult(year) && !IsMarried(year);
}

bool Person::IsMarried(const int year) const
{
    std::lock_guard<std::recursive_mutex> lock(m_mutexSelf);
    return (year >= m_yearOfMarriage) && (m_spouse != nullptr);
}

int Person::Age(const int year) const
{
    std::lock_guard<std::recursive_mutex> lock(m_mutexSelf);
    return year - m_yearOfBirth;
}

bool Person::IsEmployed() const
{
    std::lock_guard<std::recursive_mutex> lock(m_mutexSelf);
    return m_profession.m_type != EProfession::None;
}

bool Person::IsProfessional() const
{
    std::lock_guard<std::recursive_mutex> lock(m_mutexSelf);
    return IsEmployed() && m_profession.m_type != EProfession::Peasant;
}