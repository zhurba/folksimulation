#pragma once

#include <string>

enum class EProfession
{
    Artist,
    Baker,
    Barber,
    Bathkeeper,
    Beermerchant,
    Blacksmith,
    Bleacher,
    Bricklayer,
    Bucklemaker,
    Butcher,
    Candlemaker,
    Carpenter,
    Carpetmaker,
    Confectioner,
    Coppersmith,
    Cutler,
    Doctor,
    Fishmonger,
    Furrier,
    Glovemaker,
    Guard,
    Handmaiden,
    Harnessmaker,
    Hatter,
    Haymerchant,
    Healer,
    Hotelkeeper,
    Jeweler,
    Locksmith,
    Oldcloth,
    Peasant,
    Plasterer,
    Potter,
    Priest,
    Pursemaker,
    Roofer,
    Ropemaker,
    Saddler,
    Scabbardmaker,
    Scribe,
    Sculptor,
    Shoemaker,
    Silktrader,
    Spicemerchant,
    Tailor,
    Tanner,
    Tavern,
    Watercarrier,
    Weaver,
    Winemerchant,
    Woodmerchant,
    Count,
    None
};

class Profession
{
public:
    Profession(std::string name, EProfession type, int supportNumber);
    ~Profession();

    std::string m_name;
    EProfession m_type;
    int m_supportNumber;
};

// Compare by number of vacancies:
struct ProfessionCompare
{
    bool operator()(const std::pair<Profession, float>& p1, const std::pair<Profession, float>& p2)
    {
        if (p1.second == p2.second)
            return p1.first.m_type < p2.first.m_type;
        else
            return p1.second < p2.second;
    }
};