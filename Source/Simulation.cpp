#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <algorithm>
#include <future>
#include <thread>

#include "Simulation.h"
#include "Social.h"

Simulation::Simulation()
{
}


Simulation::~Simulation()
{
}

void Simulation::GenerateStartPopulation(const int countOfPeople)
{
    int year = 0;

    // Generate people:
    for (int i = 0; i < countOfPeople; ++i)
    {
        std::shared_ptr<Person> person = Social::GeneratePerson(year, true);
        AddPerson(person, year);
    }

    GenerateMarriages(year);
    GainProfessions(year);
}

void Simulation::AddPerson(std::shared_ptr<Person> person, const int year)
{
    m_people.insert(person);
    m_alive.insert(person);
    m_unemployed.push_back(std::make_shared<EmploymentCard>(person, m_unemployed.size()));
    if (person->m_gender == Gender::Female)
    {
        m_singleWomen.insert(person);
    }
    else
    {
        m_singleMen.insert(person);
    }
}

void Simulation::GenerateBirths(const int year)
{
    for (auto& marriage : m_marriages)
    {
        std::shared_ptr<Person> person = marriage->ProduceChild(year);
        if (person)
        {
            AddPerson(person, year);
        }
    }
}

void Simulation::GenerateMarriages(const int year)
{
    // todo: concurrency here:
    auto iter = m_singleMen.begin();
    while (iter != m_singleMen.end())
    {
        std::shared_ptr<Marriage> marriage = Social::Marry(*iter, m_singleWomen, year);
        if (marriage)
        {
            iter = m_singleMen.erase(iter);
            m_singleWomen.erase(marriage->m_wife);
            m_marriages.insert(marriage);
        }
        else
        {
            ++iter;
        }
    }
}

void Simulation::CalcVacancies()
{
    // Recalculate number of vacancies:
    const int numOfPeople = m_alive.size();
    for (auto& pair : g_professionsToVacancies)
    {
        pair.second = numOfPeople / (float)pair.first.m_supportNumber - m_numberOfProfessionals[pair.first.m_type];
    }

    // Sort vacancies:
    std::sort(g_professionsToVacancies.begin(), g_professionsToVacancies.end(), ProfessionCompare());
}

Profession Simulation::GiveProfession()
{
    for (auto& pair : g_professionsToVacancies)
    {
        if (pair.second >= 1)
        {
            --pair.second;
            return pair.first;
        }
    }

    return g_peasant;
}

bool Simulation::GainProfession(std::shared_ptr<Person> person, const int year)
{
    if (person->IsAlive(year) && person->IsAdult(year))
    {
        person->m_profession = GiveProfession();
        ++m_numberOfProfessionals[person->m_profession.m_type];
        return true;
    }
    return false;
}

void Simulation::GainProfessions(const int year)
{
    CalcVacancies();

    for (size_t i = 0; i < m_unemployed.size(); ++i)
    {
        auto [person, queueOrder] = *m_unemployed[i];
        if (GainProfession(person, year))
        {
            m_unemployed[i]->m_person = nullptr;
            m_employed.push_back(std::make_shared<EmploymentCard>(person, m_employed.size()));
        }
    }
    ClearLaborRemains(m_unemployed);
}

void Simulation::ClearLaborRemains(std::deque<std::shared_ptr<EmploymentCard>>& container)
{
    auto predicate = [](const std::shared_ptr<EmploymentCard>& card)
        {
            return card->m_person == nullptr;
        };
    container.erase(std::remove_if(container.begin(), container.end(), predicate), container.end());
    for (size_t i = 0; i < container.size(); ++i)
    {
        container[i]->m_queueOrder = i;
    }
}

void Simulation::RemovePerson(std::shared_ptr<Person> person)
{
    std::lock_guard<std::mutex> lock(m_mutexRemove);
    m_alive.erase(person);
}

void Simulation::Retire(const int year)
{
    auto RemoveUnemployed = [this, year]()
        {
            for (size_t i = 0; i < m_unemployed.size(); ++i)
            {
                auto [person, queueOrder] = *m_unemployed[i];
                if (!person->IsAlive(year))
                {
                    RemovePerson(person);
                }
                m_unemployed[i]->m_person = nullptr;
            }
            ClearLaborRemains(m_unemployed);
        };

    auto RemoveEmployed = [this, year]()
        {
            for (size_t i = 0; i < m_employed.size(); ++i)
            {
                auto [person, queueOrder] = *m_employed[i];
                if (!person->IsAlive(year))
                {
                    RemovePerson(person);
                }
                --m_numberOfProfessionals[person->m_profession.m_type];
                m_employed[i]->m_person = nullptr;
            }
            ClearLaborRemains(m_employed);
        };

    std::future<void> futureEmployed = std::async(RemoveEmployed);
    std::future<void> futureUnemployed = std::async(RemoveUnemployed);

    futureEmployed.get();
    futureUnemployed.get();
}

void Simulation::GeneratePopulation(const int countOfPeople, const int countOfYears)
{
    m_ready = false;
    GenerateStartPopulation(countOfPeople);
    for (int year = 0; year <= countOfYears; ++year)
    {
        std::lock_guard<std::mutex> lock(m_mutexGeneration);

        auto timeBirth = std::chrono::system_clock::now();

        GenerateBirths(year);

        auto timeMarriage = std::chrono::system_clock::now();

        GenerateMarriages(year);

        auto timeRetire = std::chrono::system_clock::now();

        Retire(year);

        auto timeProfession = std::chrono::system_clock::now();

        GainProfessions(year);

        auto timeComplete = std::chrono::system_clock::now();


        std::chrono::duration<double> durBirth = timeMarriage - timeBirth;
        std::chrono::duration<double> durMarriage = timeRetire - timeMarriage;
        std::chrono::duration<double> durRetire = timeProfession - timeRetire;
        std::chrono::duration<double> durProfession = timeComplete - timeProfession;
        std::cout << "GenerateBirths (year " << year << ") " << durBirth.count() << std::endl;
        std::cout << "GenerateMarriages (year " << year << ") " << durMarriage.count() << std::endl;
        std::cout << "Retire (year " << year << ") " << durRetire.count() << std::endl;
        std::cout << "GainProfessions (year " << year << ") " << durProfession.count() << std::endl;
    }
    m_ready = true;
    m_cvPrint.notify_one();
}

void Simulation::PrintCommand(const std::string& commandLine) const
{
    std::unique_lock<std::mutex> lock(m_mutexGeneration);
    std::cout << "PrintCommand in action... \n";
    m_cvPrint.wait(lock, [this]{ return m_ready; });
    if (commandLine.empty())
    {
        return;
    }

    size_t startPos = 0;
    size_t endPos = 0;
    std::vector<std::string> args;
    args.reserve(10);
    do
    {
        endPos = commandLine.find(' ', startPos);
        if (endPos == std::string::npos)
        {
            args.push_back(commandLine.substr(startPos));
            break;
        }
        args.push_back(commandLine.substr(startPos, endPos - startPos));
        ++endPos;
        startPos = endPos;
    } while (endPos < commandLine.size());

    if (args.empty())
    {
        return;
    }

    std::string command = args[0];
    int year = 0;
    bool alive = false;
    bool professionals = false;
    for (size_t i = 1; i < args.size(); ++i)
    {
        int intVal = atoi(args[i].c_str());
        if (intVal)
        {
            year = intVal;
        }
        if (args[i] == "alive")
        {
            alive = true;
        }
        if (args[i] == "prof")
        {
            professionals = true;
        }
    }

    if (command == "people")
    {
        PrintPopulation(year, alive, professionals);
    }
    else if (command == "pairs")
    {
        PrintPairs(year, alive);
    }
    else if (command == "person")
    {
        PrintPerson(args.back());
    }
}

void Simulation::PrintPopulation(const int year, const bool alive, const bool professionals) const
{
    int population = 0;
    for (auto& person : m_people)
    {
        if (alive && !person->IsAlive(year))
            continue;

        if (professionals && !person->IsProfessional())
            continue;

        ++population;

        std::cout << std::setw(15) << person->m_name
                  << std::setw(7) << Social::GenderToString(person->m_gender)
                  << std::setw(9) << "(age " << person->Age(year) << ")"
                  << std::setw(8) << (person->IsMarried(year) ? "married" : "single");
        //<< std::setw(6) << Social::StatusToString(person->m_status)

        if (person->IsEmployed())
            std::cout << std::setw(15) << "(prof. " << person->m_profession.m_name << ")";

        std::cout << std::endl;
    }

    std::cout << "Year = " << year << std::endl;
    std::cout << "Listed = " << population << std::endl;
    std::cout << "Total = " << m_people.size() << std::endl;
}

void Simulation::PrintPairs(const int year, const bool alive) const
{
    for (auto& marriage : m_marriages)
    {
        if (marriage->m_husband->m_yearOfMarriage > year)
            continue;

        if (alive && !(marriage->m_husband->IsAlive(year) && marriage->m_wife->IsAlive(year)))
            continue;

        std::cout << std::setw(12) << marriage->m_husband->m_name << "(age "<< marriage->m_husband->Age(year) << ") + "
                  << std::setw(12) << marriage->m_wife->m_name << "(age " << marriage->m_wife->Age(year) << ")"
                  << std::endl;
    }
}

void Simulation::PrintPerson(const std::string& name) const
{
    std::vector<std::shared_ptr<Person>> candidates;
    candidates.reserve(m_people.size());
    auto iter = m_people.begin();
    auto findByName = [name](const std::shared_ptr<Person> & person) { return person->m_name == name; };
    while ((iter = std::find_if(iter, m_people.end(), findByName)) != m_people.end())
    {
        candidates.push_back(*iter);
        ++iter;
    }

    if (candidates.empty())
    {
        return;
    }

    std::shared_ptr<Person> person;
    if (candidates.size() > 1)
    {
        for (size_t i = 0; i < candidates.size(); ++i)
        {
            std::cout << std::setw(3) << (i + 1) << ". ";
            PrintPerson(candidates[i], false);
        }
        std::cout << "Choose the appropriate candidate [1-" << candidates.size() << "]:";
        int choice = 0;
        std::cin >> choice;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        if (choice < 1)
        {
            choice = 1;
        }
        if (choice > candidates.size())
        {
            choice = candidates.size();
        }
        person = candidates[choice - 1];
    }
    else
    {
        person = candidates[0];
    }

    PrintPerson(person, true);
    if (person->m_father)
    {
        std::cout << "Father: ";
        PrintPerson(person->m_father, false);
    }
    if (person->m_mother)
    {
        std::cout << "Mother: ";
        PrintPerson(person->m_mother, false);
    }
    if (person->m_spouse)
    {
        std::cout << "Spouse: ";
        PrintPerson(person->m_spouse, false);
    }
    if (person->m_marriage)
    {
        std::cout << "Children:\n";
        for (auto& child : person->m_marriage->m_children)
        {
            std::cout << " ";
            PrintPerson(child, false);
        }
    }
}

void Simulation::PrintPerson(const std::shared_ptr<Person>& person, const bool details) const
{
    if (!person)
    {
        return;
    }

    if (details)
    {
        std::cout << "Name: " << person->m_name << std::endl;
        std::cout << "Gender: " << Social::GenderToString(person->m_gender) << std::endl;
        std::cout << "Years : " << person->m_yearOfBirth << "-" << person->m_yearOfDeath << std::endl;
        std::cout << "Marital status: ";
        if (person->m_spouse)
        {
            std::cout << "married (year of marriage: " << person->m_yearOfMarriage << ")" << std::endl;
        }
        else
        {
            std::cout << "single" << std::endl;
        }
        if (person->IsEmployed())
        {
            std::cout << "Profession: " << person->m_profession.m_name << std::endl;
        }
    }
    else
    {
        std::cout << person->m_name;
        std::cout << " (" << Social::GenderToString(person->m_gender) << ", ";
        std::cout << person->m_yearOfBirth << "-" << person->m_yearOfDeath << ", ";
        if (person->m_spouse)
        {
            std::cout << "married (" << person->m_yearOfMarriage << "), ";
        }
        else
        {
            std::cout << "single, ";
        }
        if (person->IsEmployed())
        {
            std::cout << "profession: " << person->m_profession.m_name << ")"<< std::endl;
        }
        else
        {
            std::cout << "no profession)" << std::endl;
        }
    }
}