#pragma once

#include <ctime>
#include <iostream>
#include "Simulation.h"


void main()
{
    srand(time(0));

    int population;
    int years;
    std::cout << "Enter number of start population for simulation:";
    std::cin >> population;
    std::cout << "Enter number of years for simulation:";
    std::cin >> years;

    Simulation s;


    //std::jthread t([&s, population, years]()
        //{
        s.GeneratePopulation(population, years);
        //});

    std::cout << "Commands available:\n";
    std::cout << "  people [<year>][alive][prof]\n";
    std::cout << "  pairs [<year>][alive]\n";
    std::cout << "  person <name>\n";
    std::cout << "  exit\n";

    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::string command;
    do
    {
        std::cout << "Enter command:\n";
        getline(std::cin, command);
        s.PrintCommand(command);
    } while (command != "exit");
}