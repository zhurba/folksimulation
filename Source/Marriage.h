#pragma once

#include <memory>
#include <set>
#include "Person.h"

class Marriage
{
public:
    Marriage(std::shared_ptr<Person> husband, std::shared_ptr<Person> wife, const int year);
    ~Marriage();

    std::shared_ptr<Person> ProduceChild(const int year);

    std::shared_ptr<Person> m_husband;
    std::shared_ptr<Person> m_wife;
    std::set<std::shared_ptr<Person>> m_children;
    int m_maxChildren;
};

