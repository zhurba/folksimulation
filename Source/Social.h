#pragma once

#include <memory>
#include <string>
#include <set>
#include "Data.h"
#include "Marriage.h"

class Social
{
public:
    Social();
    ~Social();

    static std::string GetRandomName(Gender gender);
    static Gender GetRandomGender();
    static int GetRandomAge(int maxAge);
    static int GetRandomMaxAge();
    static Status GetRandomStatus();
    static std::string GenderToString(Gender gender);
    static std::string StatusToString(Status status);
    static std::shared_ptr<Person> GeneratePerson(int yearOfBirth, const bool isStartPopulation);
    static std::shared_ptr<Marriage> Marry(std::shared_ptr<Person> husband, std::set<std::shared_ptr<Person>, PersonCompareByAge> wifes, const int year);
};

