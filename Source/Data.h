#pragma once

#include <string>
#include <vector>
#include <set>

#include "Profession.h"

const int g_maturityAge = 16;
const int g_maxMarriageDiff = 5;
const int g_maxBabyBirthAge = 40;
const int g_maxChildrenTotal = 5;

static Profession g_peasant("Peasant", EProfession::Peasant, 0);
static std::vector<std::pair<Profession, float>> g_professionsToVacancies =
{
    {Profession("Artist", EProfession::Artist, 1500), 0},
    {Profession("Baker", EProfession::Baker, 800), 0},
    {Profession("Barber", EProfession::Barber, 350), 0 },
    {Profession("Bathkeeper", EProfession::Bathkeeper, 1900), 0},
    {Profession("Beermerchant", EProfession::Beermerchant, 1400), 0},
    {Profession("Blacksmith", EProfession::Blacksmith, 1500), 0},
    {Profession("Bleacher", EProfession::Bleacher, 2100), 0},
    {Profession("Bricklayer", EProfession::Bricklayer, 500), 0},
    {Profession("Bucklemaker", EProfession::Bucklemaker, 1400), 0},// steel on clothes
    {Profession("Butcher", EProfession::Butcher, 1200), 0},// meat
    {Profession("Candlemaker", EProfession::Candlemaker, 700), 0},
    {Profession("Carpenter", EProfession::Carpenter, 550), 0},// wooden goods
    {Profession("Carpetmaker", EProfession::Carpetmaker, 2000), 0},
    {Profession("Confectioner", EProfession::Confectioner, 500), 0},// candy
    {Profession("Coppersmith", EProfession::Coppersmith, 700), 0},
    {Profession("Cutler", EProfession::Cutler, 2300), 0},// knives
    {Profession("Doctor", EProfession::Doctor, 1700), 0},
    {Profession("Fishmonger", EProfession::Fishmonger, 1200), 0},// trade fish
    {Profession("Furrier", EProfession::Furrier, 250), 0},// fur
    {Profession("Glovemaker", EProfession::Glovemaker, 2400), 0},
    {Profession("Guard", EProfession::Guard, 150), 0},
    {Profession("Handmaiden", EProfession::Handmaiden, 250), 0},
    {Profession("Harnessmaker", EProfession::Harnessmaker, 2000), 0},// for horses
    {Profession("Hatter", EProfession::Hatter, 950), 0},
    {Profession("Haymerchant", EProfession::Haymerchant, 2300), 0},// food for animals
    {Profession("Healer", EProfession::Healer, 350), 0},
    {Profession("Hotelkeeper", EProfession::Hotelkeeper, 2000), 0},
    {Profession("Jeweler", EProfession::Jeweler, 400), 0},
    {Profession("Locksmith", EProfession::Locksmith, 1900), 0},
    {Profession("Oldcloth", EProfession::Oldcloth, 400), 0},
    {Profession("Plasterer", EProfession::Plasterer, 1400), 0},
    {Profession("Potter", EProfession::Potter, 400), 0},// clay; estimate
    {Profession("Priest", EProfession::Priest, 100), 0},
    {Profession("Pursemaker", EProfession::Pursemaker, 1100), 0},// bags
    {Profession("Roofer", EProfession::Roofer, 1800), 0},
    {Profession("Ropemaker", EProfession::Ropemaker, 1900), 0},
    {Profession("Saddler", EProfession::Saddler, 1000), 0},// to sit on horse
    {Profession("Scabbardmaker", EProfession::Scabbardmaker, 850), 0},// for holding knives/swords
    {Profession("Scribe", EProfession::Scribe, 2000), 0},// books
    {Profession("Sculptor", EProfession::Sculptor, 2000), 0},
    {Profession("Shoemaker", EProfession::Shoemaker, 150), 0},
    {Profession("Silktrader", EProfession::Silktrader, 700), 0},
    {Profession("Spicemerchant", EProfession::Spicemerchant, 1400), 0},
    {Profession("Tailor", EProfession::Tailor, 250), 0},
    {Profession("Tanner", EProfession::Tanner, 2000), 0},// skin
    {Profession("Tavern", EProfession::Tavern, 400), 0},
    {Profession("Watercarrier", EProfession::Watercarrier, 850), 0},
    {Profession("Weaver", EProfession::Weaver, 600), 0},// textile
    {Profession("Winemerchant", EProfession::Winemerchant, 900), 0},
    {Profession("Woodmerchant", EProfession::Woodmerchant, 2400), 0}
};

static std::vector<std::string> femaleNames =
{
    "Adele",
    "Albina",
    "Amber",
    "Ardis",
    "Brenda",
    "Bridget",
    "Bronach",
    "Calla",
    "Caoimhe",
    "Caroline",
    "Casey",
    "Cassidy",
    "Cathleen",
    "Charlotte",
    "Chloe",
    "Colleen",
    "Danielle",
    "Daphne",
    "Diana",
    "Earlene",
    "Edna",
    "Erin",
    "Fidelma",
    "Flann",
    "Gemma",
    "Georgia",
    "Grace",
    "Heather",
    "Helena",
    "Holly",
    "Imelda",
    "Jack",
    "Jacqueline",
    "Jacqui",
    "Karen",
    "Kathleen",
    "Keira",
    "Mairead",
    "Mary",
    "Maura",
    "Maureen",
    "Melanie",
    "Moira",
    "Mor",
    "Nadia",
    "Nora",
    "Oona",
    "Padraigin",
    "Patricia",
    "Pegeen",
    "Roisin",
    "Rosaleen",
    "Sadhbh",
    "Shauna",
    "Sinead",
    "Sorcha",
    "Tara",
    "Valerie",
    "Veronica"
};

static std::vector<std::string> maleNames =
{
    "Abraham",
    "Adam",
    "Aidan",
    "Alexander",
    "Alistair",
    "Angus",
    "Antoin",
    "Arthur",
    "Broderick",
    "Bruno",
    "Caden",
    "Calvin",
    "Carson",
    "Casey",
    "Christian",
    "Christy",
    "Colin",
    "Colman",
    "Conal",
    "Conle",
    "Connor",
    "Conor",
    "Conway",
    "Dallan",
    "Darragh",
    "Declan",
    "Dirk",
    "Dougal",
    "Eamonn",
    "Euan",
    "Ewan",
    "Fearghal",
    "Felix",
    "Fergal",
    "Fergus",
    "Finn",
    "Flann",
    "Francie",
    "Gerald",
    "Harold",
    "Heber",
    "Ian",
    "Ivan",
    "Jack",
    "Jacob",
    "John",
    "Jonathan",
    "Kayle",
    "Keenan",
    "Kennedy",
    "Kevin",
    "Kian",
    "Killian",
    "Liam",
    "Loughlin",
    "Mael Brigte",
    "Matthew",
    "Micheal",
    "Neil",
    "Nevan",
    "Oisin",
    "Orin",
    "Oscar",
    "Ossian",
    "Patrick",
    "Patsy",
    "Pauric",
    "Philip",
    "Ralph",
    "Robert",
    "Ronan",
    "Rory",
    "Ryan",
    "Samuel",
    "Seamus",
    "Sean",
    "Shane",
    "Sorley",
    "Thomas",
    "Trevor",
    "Tristan",
    "Tyrone",
    "Uilleac",
    "Uilleag",
    "Ulick",
    "Zayden"
};